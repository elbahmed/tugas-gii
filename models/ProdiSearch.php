<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Prodi;

/**
 * ProdiSearch represents the model behind the search form of `app\models\Prodi`.
 */
class ProdiSearch extends Prodi
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prodi_id'], 'integer'],
            [['prodi_code', 'prodi_name', 'prodi_address', 'prodi_phone', 'prodi_cheif'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Prodi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'prodi_id' => $this->prodi_id,
        ]);

        $query->andFilterWhere(['like', 'prodi_code', $this->prodi_code])
            ->andFilterWhere(['like', 'prodi_name', $this->prodi_name])
            ->andFilterWhere(['like', 'prodi_address', $this->prodi_address])
            ->andFilterWhere(['like', 'prodi_phone', $this->prodi_phone])
            ->andFilterWhere(['like', 'prodi_cheif', $this->prodi_cheif]);

        return $dataProvider;
    }
}

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PenelitianDosen */

$this->title = 'Update Penelitian Dosen: ' . $model->penelitian_dosen_id;
$this->params['breadcrumbs'][] = ['label' => 'Penelitian Dosen', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->penelitian_dosen_id, 'url' => ['view', 'id' => $model->penelitian_dosen_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="penelitian-dosen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

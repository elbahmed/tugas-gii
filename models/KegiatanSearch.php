<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kegiatan;

/**
 * KegiatanSearch represents the model behind the search form of `app\models\Kegiatan`.
 */
class KegiatanSearch extends Kegiatan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kegiatan_id', 'kegiatan_score', 'kegiatan_jenis_kegiatan_id', 'kegiatan_dosen_id'], 'integer'],
            [['kegiatan_dos', 'kegiatan_doe', 'kegiatan_place', 'kegiatan_desc', 'kegiatan_membership'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kegiatan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'kegiatan_id' => $this->kegiatan_id,
            'kegiatan_dos' => $this->kegiatan_dos,
            'kegiatan_doe' => $this->kegiatan_doe,
            'kegiatan_score' => $this->kegiatan_score,
            'kegiatan_jenis_kegiatan_id' => $this->kegiatan_jenis_kegiatan_id,
            'kegiatan_dosen_id' => $this->kegiatan_dosen_id,
        ]);

        $query->andFilterWhere(['like', 'kegiatan_place', $this->kegiatan_place])
            ->andFilterWhere(['like', 'kegiatan_desc', $this->kegiatan_desc])
            ->andFilterWhere(['like', 'kegiatan_membership', $this->kegiatan_membership]);

        return $dataProvider;
    }
}

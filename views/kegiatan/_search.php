<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KegiatanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kegiatan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'kegiatan_id') ?>

    <?= $form->field($model, 'kegiatan_dos') ?>

    <?= $form->field($model, 'kegiatan_doe') ?>

    <?= $form->field($model, 'kegiatan_place') ?>

    <?= $form->field($model, 'kegiatan_desc') ?>

    <?php // echo $form->field($model, 'kegiatan_membership') ?>

    <?php // echo $form->field($model, 'kegiatan_score') ?>

    <?php // echo $form->field($model, 'kegiatan_jenis_kegiatan_id') ?>

    <?php // echo $form->field($model, 'kegiatan_dosen_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

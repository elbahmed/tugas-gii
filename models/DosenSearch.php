<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Dosen;

/**
 * DosenSearch represents the model behind the search form of `app\models\Dosen`.
 */
class DosenSearch extends Dosen
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dosen_id', 'dosen_prodi_id'], 'integer'],
            [['dosen_nidn', 'dosen_name', 'dosen_degree', 'dosen_title', 'dosen_sex', 'dosen_pob', 'dosen_dob', 'dosen_address', 'dosen_email', 'dosen_doe'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dosen::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'dosen_id' => $this->dosen_id,
            'dosen_dob' => $this->dosen_dob,
            'dosen_doe' => $this->dosen_doe,
            'dosen_prodi_id' => $this->dosen_prodi_id,
        ]);

        $query->andFilterWhere(['like', 'dosen_nidn', $this->dosen_nidn])
            ->andFilterWhere(['like', 'dosen_name', $this->dosen_name])
            ->andFilterWhere(['like', 'dosen_degree', $this->dosen_degree])
            ->andFilterWhere(['like', 'dosen_title', $this->dosen_title])
            ->andFilterWhere(['like', 'dosen_sex', $this->dosen_sex])
            ->andFilterWhere(['like', 'dosen_pob', $this->dosen_pob])
            ->andFilterWhere(['like', 'dosen_address', $this->dosen_address])
            ->andFilterWhere(['like', 'dosen_email', $this->dosen_email]);

        return $dataProvider;
    }
}

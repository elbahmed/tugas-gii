<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PenelitianDosenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penelitian Dosen';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penelitian-dosen-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Penelitian Dosen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'penelitian_dosen_id',
            'penelitian_dosen_dosen_id',
            'penelitian_dosen_title:ntext',
            'penelitian_dosen_dos',
            'penelitian_dosen_doe',
            //'penelitian_dosen_academic_year',
            //'penelitian_dosen_research_team',
            //'penelitian_dosen_bidang_ilmu_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

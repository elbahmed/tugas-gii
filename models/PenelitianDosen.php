<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "penelitian_dosen".
 *
 * @property int $penelitian_dosen_id
 * @property int $penelitian_dosen_dosen_id
 * @property string $penelitian_dosen_title
 * @property string $penelitian_dosen_dos
 * @property string $penelitian_dosen_doe
 * @property string $penelitian_dosen_academic_year
 * @property string $penelitian_dosen_research_team
 * @property int $penelitian_dosen_bidang_ilmu_id
 *
 * @property BidangIlmu $penelitianDosenBidangIlmu
 * @property Dosen $penelitianDosenDosen
 */
class PenelitianDosen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'penelitian_dosen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['penelitian_dosen_dosen_id', 'penelitian_dosen_title', 'penelitian_dosen_dos', 'penelitian_dosen_doe', 'penelitian_dosen_academic_year', 'penelitian_dosen_research_team', 'penelitian_dosen_bidang_ilmu_id'], 'required'],
            [['penelitian_dosen_dosen_id', 'penelitian_dosen_bidang_ilmu_id'], 'integer'],
            [['penelitian_dosen_title'], 'string'],
            [['penelitian_dosen_dos', 'penelitian_dosen_doe'], 'safe'],
            [['penelitian_dosen_academic_year', 'penelitian_dosen_research_team'], 'string', 'max' => 255],
            [['penelitian_dosen_bidang_ilmu_id'], 'exist', 'skipOnError' => true, 'targetClass' => BidangIlmu::className(), 'targetAttribute' => ['penelitian_dosen_bidang_ilmu_id' => 'bidang_ilmu_id']],
            [['penelitian_dosen_dosen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dosen::className(), 'targetAttribute' => ['penelitian_dosen_dosen_id' => 'dosen_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'penelitian_dosen_id' => 'ID',
            'penelitian_dosen_dosen_id' => 'Dosen',
            'penelitian_dosen_title' => 'Judul',
            'penelitian_dosen_dos' => 'Tanggal Mulai',
            'penelitian_dosen_doe' => 'Tanggal Selesai',
            'penelitian_dosen_academic_year' => 'Tahun ajaran',
            'penelitian_dosen_research_team' => 'tim riset',
            'penelitian_dosen_bidang_ilmu_id' => 'Bidang Ilmu',
        ];
    }

    /**
     * Gets query for [[PenelitianDosenBidangIlmu]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPenelitianDosenBidangIlmu()
    {
        return $this->hasOne(BidangIlmu::className(), ['bidang_ilmu_id' => 'penelitian_dosen_bidang_ilmu_id']);
    }

    /**
     * Gets query for [[PenelitianDosenDosen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPenelitianDosenDosen()
    {
        return $this->hasOne(Dosen::className(), ['dosen_id' => 'penelitian_dosen_dosen_id']);
    }
}

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DosenSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dosen-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'dosen_id') ?>

    <?= $form->field($model, 'dosen_nidn') ?>

    <?= $form->field($model, 'dosen_name') ?>

    <?= $form->field($model, 'dosen_degree') ?>

    <?= $form->field($model, 'dosen_title') ?>

    <?php // echo $form->field($model, 'dosen_sex') ?>

    <?php // echo $form->field($model, 'dosen_pob') ?>

    <?php // echo $form->field($model, 'dosen_dob') ?>

    <?php // echo $form->field($model, 'dosen_address') ?>

    <?php // echo $form->field($model, 'dosen_email') ?>

    <?php // echo $form->field($model, 'dosen_doe') ?>

    <?php // echo $form->field($model, 'dosen_prodi_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

namespace app\controllers;

date_default_timezone_set('Asia/Jakarta');

use Yii;
use app\models\User;
use app\models\UserSearch;
use Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->session['user_level'] != 1) {
            return $this->redirect('/user/login');
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSignup()
    {
        $model = new \app\models\User();

        if (Yii::$app->request->isAjax) {
            $model->load(Yii::$app->request->post());
            if ($model->validate()) {
                // form inputs are valid, do something here
                $password = $model->__get('password');
                $hash = Yii::$app->getSecurity()->generatePasswordHash($password);
                $model->setAttribute('password', $hash);
                $model->setAttribute('created_time', date('Y-m-d H:i:s'));
                if($model->save()){   
                    //
                    return true;   
                }  
                return false;
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }


    public function actionLogin()
    {

        $model = new \app\models\User();

        if (Yii::$app->request->isAjax) {

            $model->load(Yii::$app->request->post());
            $username = $model->__get('username');
            try {
                $user = $model::findOne(['username' => $username]);
                $hash = $user->password;
            } catch(Exception $e) {
                return 'username salah';
            }
            $password = $model->__get('password');
            if (Yii::$app->getSecurity()->validatePassword($password, $hash)) {
                Yii::$app->session['user_level'] = $user->level;
                return true;
            }
            return 'Password salah';
        }
    
        return $this->render('login', [
            'model' => $model,
        ]);
    }

}

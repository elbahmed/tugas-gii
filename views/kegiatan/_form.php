<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kegiatan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kegiatan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kegiatan_dos')->textInput() ?>

    <?= $form->field($model, 'kegiatan_doe')->textInput() ?>

    <?= $form->field($model, 'kegiatan_place')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kegiatan_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'kegiatan_membership')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kegiatan_score')->textInput() ?>

    <?= $form->field($model, 'kegiatan_jenis_kegiatan_id')->textInput() ?>

    <?= $form->field($model, 'kegiatan_dosen_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

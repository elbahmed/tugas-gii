<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dosen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dosen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dosen_nidn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dosen_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dosen_degree')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dosen_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dosen_sex')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dosen_pob')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dosen_dob')->textInput() ?>

    <?= $form->field($model, 'dosen_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dosen_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dosen_doe')->textInput() ?>

    <?= $form->field($model, 'dosen_prodi_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BidangIlmu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bidang-ilmu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'bidang_ilmu_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bidang_ilmu_desc')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

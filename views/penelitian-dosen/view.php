<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PenelitianDosen */

$this->title = $model->penelitian_dosen_id;
$this->params['breadcrumbs'][] = ['label' => 'Penelitian Dosen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="penelitian-dosen-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->penelitian_dosen_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->penelitian_dosen_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'penelitian_dosen_id',
            'penelitian_dosen_dosen_id',
            'penelitian_dosen_title:ntext',
            'penelitian_dosen_dos',
            'penelitian_dosen_doe',
            'penelitian_dosen_academic_year',
            'penelitian_dosen_research_team',
            'penelitian_dosen_bidang_ilmu_id',
        ],
    ]) ?>

</div>

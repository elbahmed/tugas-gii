<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PenelitianDosenSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="penelitian-dosen-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'penelitian_dosen_id') ?>

    <?= $form->field($model, 'penelitian_dosen_dosen_id') ?>

    <?= $form->field($model, 'penelitian_dosen_title') ?>

    <?= $form->field($model, 'penelitian_dosen_dos') ?>

    <?= $form->field($model, 'penelitian_dosen_doe') ?>

    <?php // echo $form->field($model, 'penelitian_dosen_academic_year') ?>

    <?php // echo $form->field($model, 'penelitian_dosen_research_team') ?>

    <?php // echo $form->field($model, 'penelitian_dosen_bidang_ilmu_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

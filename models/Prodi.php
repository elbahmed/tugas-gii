<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prodi".
 *
 * @property int $prodi_id
 * @property string $prodi_code
 * @property string $prodi_name
 * @property string $prodi_address
 * @property string $prodi_phone
 * @property string $prodi_cheif
 *
 * @property Dosen[] $dosens
 */
class Prodi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prodi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prodi_code', 'prodi_name', 'prodi_address', 'prodi_phone', 'prodi_cheif'], 'required'],
            [['prodi_address'], 'string'],
            [['prodi_code', 'prodi_name', 'prodi_phone', 'prodi_cheif'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'prodi_id' => 'ID',
            'prodi_code' => 'Code',
            'prodi_name' => 'Name',
            'prodi_address' => 'Alamat',
            'prodi_phone' => 'No Telpon',
            'prodi_cheif' => 'Kepala',
        ];
    }

    /**
     * Gets query for [[Dosens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDosens()
    {
        return $this->hasMany(Dosen::className(), ['dosen_prodi_id' => 'prodi_id']);
    }
}

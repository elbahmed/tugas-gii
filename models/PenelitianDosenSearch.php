<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PenelitianDosen;

/**
 * PenelitianDosenSearch represents the model behind the search form of `app\models\PenelitianDosen`.
 */
class PenelitianDosenSearch extends PenelitianDosen
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['penelitian_dosen_id', 'penelitian_dosen_dosen_id', 'penelitian_dosen_bidang_ilmu_id'], 'integer'],
            [['penelitian_dosen_title', 'penelitian_dosen_dos', 'penelitian_dosen_doe', 'penelitian_dosen_academic_year', 'penelitian_dosen_research_team'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PenelitianDosen::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'penelitian_dosen_id' => $this->penelitian_dosen_id,
            'penelitian_dosen_dosen_id' => $this->penelitian_dosen_dosen_id,
            'penelitian_dosen_dos' => $this->penelitian_dosen_dos,
            'penelitian_dosen_doe' => $this->penelitian_dosen_doe,
            'penelitian_dosen_bidang_ilmu_id' => $this->penelitian_dosen_bidang_ilmu_id,
        ]);

        $query->andFilterWhere(['like', 'penelitian_dosen_title', $this->penelitian_dosen_title])
            ->andFilterWhere(['like', 'penelitian_dosen_academic_year', $this->penelitian_dosen_academic_year])
            ->andFilterWhere(['like', 'penelitian_dosen_research_team', $this->penelitian_dosen_research_team]);

        return $dataProvider;
    }
}

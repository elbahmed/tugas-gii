<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form ActiveForm */
?>
<div class="user-login">

    <?php $form = ActiveForm::begin(['id' => 'form']); ?>

        <?= $form->field($model, 'username') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
    
    <?php ActiveForm::end(); ?>

    <div class="form-group">
        <?= Html::button('Submit', ['class' => 'btn btn-primary', 'id' => 'btn']) ?>
    </div>

</div><!-- user-login -->

<?= 
    $this->registerJsFile(
        '@web/js/ajaxLogin.js',
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );
?>


<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dosen".
 *
 * @property int $dosen_id
 * @property string $dosen_nidn
 * @property string $dosen_name
 * @property string $dosen_degree
 * @property string $dosen_title
 * @property string $dosen_sex
 * @property string $dosen_pob
 * @property string $dosen_dob
 * @property string $dosen_address
 * @property string $dosen_email
 * @property string $dosen_doe
 * @property int $dosen_prodi_id
 *
 * @property Prodi $dosenProdi
 * @property Kegiatan[] $kegiatans
 * @property PenelitianDosen[] $penelitianDosens
 */
class Dosen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dosen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dosen_nidn', 'dosen_name', 'dosen_degree', 'dosen_title', 'dosen_sex', 'dosen_pob', 'dosen_dob', 'dosen_address', 'dosen_email', 'dosen_doe', 'dosen_prodi_id'], 'required'],
            [['dosen_dob', 'dosen_doe'], 'safe'],
            [['dosen_prodi_id'], 'integer'],
            [['email'], 'email'],
            [['dosen_nidn', 'dosen_name', 'dosen_degree', 'dosen_title', 'dosen_pob', 'dosen_address', 'dosen_email'], 'string', 'max' => 255],
            [['dosen_sex'], 'string', 'max' => 1],
            [['dosen_prodi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prodi::className(), 'targetAttribute' => ['dosen_prodi_id' => 'prodi_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dosen_id' => 'ID',
            'dosen_nidn' => 'NIDN',
            'dosen_name' => 'Name',
            'dosen_degree' => 'Gelar Belakang',
            'dosen_title' => 'Gelar Depan',
            'dosen_sex' => 'Jenis Kelamin',
            'dosen_pob' => 'Tempat Lahir',
            'dosen_dob' => 'Tanggal Lahir',
            'dosen_address' => 'Alamat',
            'dosen_email' => 'Email',
            'dosen_doe' => 'Tanggal masuk',
            'dosen_prodi_id' => 'Prodi',
        ];
    }

    /**
     * Gets query for [[DosenProdi]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDosenProdi()
    {
        return $this->hasOne(Prodi::className(), ['prodi_id' => 'dosen_prodi_id']);
    }

    /**
     * Gets query for [[Kegiatans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKegiatans()
    {
        return $this->hasMany(Kegiatan::className(), ['kegiatan_dosen_id' => 'dosen_id']);
    }

    /**
     * Gets query for [[PenelitianDosens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPenelitianDosens()
    {
        return $this->hasMany(PenelitianDosen::className(), ['penelitian_dosen_dosen_id' => 'dosen_id']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis_kegiatan".
 *
 * @property int $jenis_kegiatan_id
 * @property string $jenis_kegiatan_name
 *
 * @property Kegiatan[] $kegiatans
 */
class JenisKegiatan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenis_kegiatan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jenis_kegiatan_name'], 'required'],
            [['jenis_kegiatan_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'jenis_kegiatan_id' => 'ID',
            'jenis_kegiatan_name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Kegiatans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKegiatans()
    {
        return $this->hasMany(Kegiatan::className(), ['kegiatan_jenis_kegiatan_id' => 'jenis_kegiatan_id']);
    }
}

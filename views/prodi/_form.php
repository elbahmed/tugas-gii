<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Prodi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prodi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'prodi_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prodi_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prodi_address')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'prodi_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prodi_cheif')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

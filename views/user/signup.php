<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form ActiveForm */
?>
<div class="user-signup card" style="width: 80rem; margin: auto;">

    <?php $form = ActiveForm::begin(['id' => 'form']); ?>

        <?= $form->field($model, 'username') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'level') ?>
        <?= $form->field($model, 'created_time')->textInput(['readonly' => true, 'value' => date('D, M Y H:i:s')]) ?>
    
    <?php ActiveForm::end(); ?>

    <div class="form-group">
        <?= Html::button('Submit', ['class' => 'btn btn-primary', 'id' => 'btn']) ?>
    </div>

</div><!-- user-signup -->

<?=
    $this->registerJsFile(
        '@web/js/ajaxSignup.js',
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );
?>
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BidangIlmu */

$this->title = 'Update Bidang Ilmu: ' . $model->bidang_ilmu_id;
$this->params['breadcrumbs'][] = ['label' => 'Bidang Ilmu', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->bidang_ilmu_id, 'url' => ['view', 'id' => $model->bidang_ilmu_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bidang-ilmu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

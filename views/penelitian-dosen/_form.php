<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PenelitianDosen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="penelitian-dosen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'penelitian_dosen_dosen_id')->textInput() ?>

    <?= $form->field($model, 'penelitian_dosen_title')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'penelitian_dosen_dos')->textInput() ?>

    <?= $form->field($model, 'penelitian_dosen_doe')->textInput() ?>

    <?= $form->field($model, 'penelitian_dosen_academic_year')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'penelitian_dosen_research_team')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'penelitian_dosen_bidang_ilmu_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

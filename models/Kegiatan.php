<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kegiatan".
 *
 * @property int $kegiatan_id
 * @property string $kegiatan_dos
 * @property string $kegiatan_doe
 * @property string $kegiatan_place
 * @property string $kegiatan_desc
 * @property string $kegiatan_membership
 * @property int $kegiatan_score
 * @property int $kegiatan_jenis_kegiatan_id
 * @property int $kegiatan_dosen_id
 *
 * @property JenisKegiatan $kegiatanJenisKegiatan
 * @property Dosen $kegiatanDosen
 */
class Kegiatan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kegiatan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kegiatan_dos', 'kegiatan_doe', 'kegiatan_place', 'kegiatan_desc', 'kegiatan_membership', 'kegiatan_score', 'kegiatan_jenis_kegiatan_id', 'kegiatan_dosen_id'], 'required'],
            [['kegiatan_dos', 'kegiatan_doe'], 'safe'],
            [['kegiatan_desc'], 'string'],
            [['kegiatan_score', 'kegiatan_jenis_kegiatan_id', 'kegiatan_dosen_id'], 'integer'],
            [['kegiatan_place'], 'string', 'max' => 255],
            [['kegiatan_membership'], 'string', 'max' => 2555],
            [['kegiatan_jenis_kegiatan_id'], 'exist', 'skipOnError' => true, 'targetClass' => JenisKegiatan::className(), 'targetAttribute' => ['kegiatan_jenis_kegiatan_id' => 'jenis_kegiatan_id']],
            [['kegiatan_dosen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dosen::className(), 'targetAttribute' => ['kegiatan_dosen_id' => 'dosen_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kegiatan_id' => 'ID',
            'kegiatan_dos' => 'Tanggal Mulai',
            'kegiatan_doe' => 'Tanggal Selesai',
            'kegiatan_place' => 'Tempat',
            'kegiatan_desc' => 'Description',
            'kegiatan_membership' => 'Membership',
            'kegiatan_score' => 'Nilai',
            'kegiatan_jenis_kegiatan_id' => 'Jenis Kegiatan',
            'kegiatan_dosen_id' => 'Dosen',
        ];
    }

    /**
     * Gets query for [[KegiatanJenisKegiatan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKegiatanJenisKegiatan()
    {
        return $this->hasOne(JenisKegiatan::className(), ['jenis_kegiatan_id' => 'kegiatan_jenis_kegiatan_id']);
    }

    /**
     * Gets query for [[KegiatanDosen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKegiatanDosen()
    {
        return $this->hasOne(Dosen::className(), ['dosen_id' => 'kegiatan_dosen_id']);
    }
}

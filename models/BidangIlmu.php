<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bidang_ilmu".
 *
 * @property int $bidang_ilmu_id
 * @property string $bidang_ilmu_name
 * @property string $bidang_ilmu_desc
 *
 * @property PenelitianDosen[] $penelitianDosens
 */
class BidangIlmu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bidang_ilmu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bidang_ilmu_name', 'bidang_ilmu_desc'], 'required'],
            [['bidang_ilmu_desc'], 'string'],
            [['bidang_ilmu_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'bidang_ilmu_id' => 'ID',
            'bidang_ilmu_name' => 'Name',
            'bidang_ilmu_desc' => 'Description',
        ];
    }

    /**
     * Gets query for [[PenelitianDosens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPenelitianDosens()
    {
        return $this->hasMany(PenelitianDosen::className(), ['penelitian_dosen_bidang_ilmu_id' => 'bidang_ilmu_id']);
    }
}

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JenisKegiatan */

$this->title = 'Update Jenis Kegiatan: ' . $model->jenis_kegiatan_id;
$this->params['breadcrumbs'][] = ['label' => 'Jenis Kegiatan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->jenis_kegiatan_id, 'url' => ['view', 'id' => $model->jenis_kegiatan_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jenis-kegiatan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
